<h4 align="center">
  MarkDown
</h4>

## Description

It's text pages formed with `common-readme` command for 4th task from ReBRAIN, but the many details was maked hands.

> Beautiful things should look beautiful from all angles

## Usage

For usage 

```
    $ common-readme > README.md
```

## API

```js
var rebrainDevopsTask1 = require('rebrain-devops-task1')
```

See [api_formatting.md](api_formatting.md) for tips.

## Install

1. Install `npm` in your Linux machine (for example Red Hat Enterprise Linux):

```
    $ yum install npm
```

2. With `npm` help install `common-readme`

```
    $ npm install -g common-readme
```

3. Usage command `common-readme` to create README.md file

```
    $ common-readme > README.md
```

For more details your may visit the site [Common README.md template](https://github.com/hackergrrl/common-readme)

## Acknowledgments

rebrain-devops-task1 was inspired by..

## See Also

- [`noffle/common-readme`](https://github.com/noffle/common-readme)
- article on Yandex.Dzen [Как написать красивый и информативный README.md](https://zen.yandex.ru/media/nuancesprog/kak-napisat-krasivyi-i-informativnyi-readmemd-5f1c1bcadae5eb15901cb5db)
- article [Правила оформления файла README.MD на GITHUB](http://webdesign.ru.net/article/pravila-oformleniya-fayla-readmemd-na-github.html)

## License

GNU